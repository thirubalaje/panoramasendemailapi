const getMailCorn=require('../controllers').getMailCorn;

module.exports = (app)=>{
    app.get('/api',(req,res)=>res.status(200).send({
        message:'Welcome to the Todos API!',
    }));
    app.get('/api/panorama/runmailcron',getMailCorn.getMailCorn)
}