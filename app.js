const express = require('express');
const bodyParser = require('body-parser');
const userController = require('./controllers');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require('./routes')(app);
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to the beginning of nothingness.',
}));

app.listen(3001, () => console.log('Example app listening on port 3001!'));

module.exports = app;