var nodemailer = require("nodemailer");
var proxy = require('express-http-proxy');
var app = require('express')();
var request = require('request');
var cron = require('node-cron');
var passwordTemplate = require('./utils/paswordreset.js');
var http = require('http');
var proxyUrl = "";

var proxiedRequest = request.defaults({ 'proxy': proxyUrl });

var getMailCorn = function () {
  cron.schedule('*/1 * * * *', function(){
    console.log('running a task every minute');
    getMailDetails();
    // getDummyEmail();
  });
}
var getMailDetails = function () {
  console.log("GetMailDetails");
  var url = "https://rntbcipanorama.rntbciconnect.com/api/panorama/emailcredential/RNTBCI"
  proxiedRequest.get(url, function (err, resp, body) {
    var jsonBody = JSON.parse(body);
    console.log("JSONDATA:" + jsonBody);
    if (jsonBody !== null && jsonBody !== '') {
      for (var i = 0; i < jsonBody.length; i++) {
        emalFormat(jsonBody[i].empfirstname, jsonBody[i].emplastname, jsonBody[i].credential, jsonBody[i].emailid, jsonBody[i].regionempid);
      }
      console.log("JSONDATA" + JSON.stringify(jsonBody));
    } else {
      console.log("JSONEmpty:" + JSON.stringify(jsonBody));
    }
  });

}
var getDummyEmail = function () {
  emalFormat('venu', 'gopal', 'Ajwp99', 'venu.gsvpn@gmail.com', 'RN08998');
}
var smtpTransport = nodemailer.createTransport({
  // host: "smtp.renault.fr",
  host: "10.171.100.100",
  port: 25 // 8025, 587 and 25 can also be used. 
  //domain:"rntbci.com",// Remove if it is not working
});

var emalFormat = (firstname, lastname, credential, emailAddress, regionempid) => {
  console.log("Entered1");
  smtpTransport.sendMail({
    from: "thiru-balaje.thirupathysamy@rntbci.com",
    to: emailAddress,
    subject: "Panorama - Reset Password Request",

    html: passwordTemplate.htmlText.replace('<USERNAME>', firstname + ' ' + lastname).replace('<PASSWORD>', credential)
  }, function (error, response) {
    console.log("Entered2");
    if (error) {
      console.log("MailSent:" + error);
    } else {
      console.log("Message sent: " + response.data);
      emaildatasaveDB(regionempid)
    }
  });
}

var emaildatasaveDB = (regionempid) => {
  var mailbody = { empid: regionempid, loginuser: "mailscheduler" }
  console.log("Regionemp:" + regionempid);
  var postUrl = "https://rntbcipanorama.rntbciconnect.com/api/panorama/emailcredentialupdate/"
  proxiedRequest.post({ url: postUrl, json: true, body: mailbody }, function (err, resp, body) {
  });
}

module.exports = {
  getMailCorn: getMailCorn
}