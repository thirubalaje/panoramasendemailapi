var htmlText = '<!DOCTYPE html>'+
'<body>'+
'<p>Dear <USERNAME>,</p>'+
'<p>You have recently requested to reset your password for your account in Panorama Tool. </p>'+
'<p>Your temporary login password is <font color="red"><PASSWORD></font></p>'+
'<p>For any issues please reach out to our support team <a href="thiru-balaje.thirupathysamy@rntbci.com">thiru-balaje.thirupathysamy@rntbci.com</a></p>'+
'<p><b> Regards, </b></p> '+
'<b> PanoramaTeam</b>'+
'</body></html>';

module.exports = {
    htmlText: htmlText
}